package LinearAlgebra;

public class Test {

	public static void main(String[] args) {
		Vector3d main = new Vector3d(1, 2, 3);
		Vector3d input = new Vector3d(3, 2, 1);
		
		System.out.println(main.magnitude());
		
		System.out.println(main.dotProduct(input));
		
		Vector3d addition = main.add(input);
		System.out.println(addition.getX());
		System.out.println(addition.getY());
		System.out.println(addition.getZ());

	}

}
