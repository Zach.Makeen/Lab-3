package LinearAlgebra;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Vector3dTests {

	@Test
	
	void getTest() {
		Vector3d get = new Vector3d(1, 2, 3);
		assertEquals(1, get.getX());
		assertEquals(2, get.getY());
		assertEquals(3, get.getZ());

	}
	
	@Test
	
	void magnitudeTest() {
		Vector3d mag = new Vector3d(1, 2, 3);
		assertEquals(3.7416573867739413, mag.magnitude());
		
	}
	
	@Test
	
	void dotProductTest() {
		Vector3d dotOne = new Vector3d(1, 2, 3);
		Vector3d dotTwo = new Vector3d(2, 3, 4);
		assertEquals(20.0, dotOne.dotProduct(dotTwo));
		
	}
	
	@Test
	
	void addTest() {
		Vector3d addOne = new Vector3d(1, 2, 3);
		Vector3d addTwo = new Vector3d(2, 3, 4);
		Vector3d newAdd = addOne.add(addTwo);
		assertEquals(3, newAdd.getX());
		assertEquals(5, newAdd.getY());
		assertEquals(7, newAdd.getZ());
		
	}

}
