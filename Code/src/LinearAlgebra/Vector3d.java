package LinearAlgebra;
import java.lang.Math;

public class Vector3d {
	double x;
	double y;
	double z;
	public Vector3d(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
		
	}
	public double getX() {
		return this.x;
		
	}
	public double getY() {
		return this.y;
		
	}
	public double getZ() {
		return this.z;
		
	}
	public double magnitude() {
		double magnitude = Math.sqrt((Math.pow(this.x, 2)) + (Math.pow(this.y, 2)) + (Math.pow(this.z, 2)));
		return magnitude;
		
	}
	public double dotProduct(Vector3d input) {
		double product = ((this.x * input.getX()) + (this.y * input.getY()) + (this.z * input.getZ()));
		return product;
		
	}
	public Vector3d add(Vector3d input) {
		Vector3d newVector = new Vector3d((this.x + input.getX()), (this.y + input.getY()), (this.z + input.getZ()));
		return newVector;
		
	}
}
